# Sibank-Parser für den Export in schulische Software
## Generelle Funktionsweise
Die Software liest die Dateien aus Indiware, LEB und Sibank Plus ein (diese liegen im Verzeichnis `Daten`).
Daraus werden Objekte für Schüler:innen und Lehrkräfte erzeugt und in den Klassen `schuelerschaft` und `kollegium` verwaltet. So werden zum Beispiel das Altern der Personen berechnet.
Vor allem aber werden die Daten aus den drei o.g. Softwarelösungen kombiniert:

- Persönliche Daten aus Sibank
  - Lehrkräfte und
  - Schülerinnen

Bei Schüler:innen:
- Klassenzugehörigkeiten von JG 5 bis 13 aus **Sibank**
- Kurszugehörigkeiten von JG 5 bis 10 aus **LEB**
- Kurszugehörigkeiten aus **Indiware**
  - Unterscheidung zwischen Klassen und Kursen in Jahrgang 11
  - Kurse ab JG 11

Bei Lehrkräften:
- Klassenzugehörigkeiten, in den eine Lehrkraft unterrichtet
- Kurszugehörgkeiten (JG 5-13)

## Damit ergeben sich Möglichkeiten zum Export
### Sibank und Untis zu IServ

Dies ist die eigentliche Hauptfunktion. An unserer Schule werden die Kurse der Sek I in LEB verwaltet, die der Sek II in Indiware, die Schüler:innen aber in Sibank Plus.
Die Kernidee hinter dieser Software war es daher, diese drei Datenbanken zu *einer* Quelle zusammenzufügen und notwendige Datenexporte zu ermöglichen.

Primär ging es uns daher darum, in IServ die Kurse vollautomatisch anzulegen und mit einem Re-Import zu aktualieren (wenn z.B. ein Schüler eine Klasse wechselt o.Ä.)

Die Software generiert pro Schüler:in eine Zeile CSV, hier exemplarisch zwei Schüler und die Kopfzeile der Datei

```csv
Import-ID;Vorname;Nachnamenszusatz;Nachname;Klasse/Information;Gruppen

2020101414453448;Max;von;Mustermann;06B2;"GRS_06_Kunst_KU05;SZE_06_Sport_07_SZE;MUE_06_Werte_und_Normen_2;LM_06_WPK_Informatik_IF_1;shu_06_WPK_Technik_TE_2_Di_1_2;Schulzweig-RZ;Schueler-Sek-I;"

2020101414455950;Erik;zum;Berge;12;"SF05;WNG7;ENG5;POG5;MAG3;SNn1;DEG3;EKL5;BIL7;SPL3;Schulzweig-GZ;Schueler-Sek-II;"

```

Bei unserer Schule sind es circa 600 Gruppen, die so automatisch erzeugt werden.


### Sibank zu Addressbuch
Aus den Lehrerdaten erzeugt die Software eine CSV-Datei, die in einem CardDAV-Adressbuch (z.B. auf einen NAS) in der Verwaltung importiert werden kann. An den Arbeitsplatzrechnern hat man so immer die aktuellen Adressdaten in der EMailssoftware etc.

Die Datei enthält pro Lehrkraft eine Zeile in diesem Stil

```csv
Musterfrau,Carina,1983-01-17,W,Mandelweg 11,26160,Bad Zwischenahn
```

### Kurslisten für TeacherTool, Meine Klassenmappe u.Ä.

Die Software generiert pro Kurs und Klasse zudem eine Teilnehmerliste. 

```csv
Vorname;Nachname;Geburtsdatum;Klasse;Geschlecht
Tom;Brady;10.01.2003;12;W
Julien;Edelmann;27.11.2003;12;M
Aaron;Rodgers;30.12.2003;12;W
Joe;Montana;25.07.2004;12;M
```

Diese Dateien können problemlos in Notensoftware eingelesen werden und von Kollegen genutzt werden.
Wir haben ein Skript, das solche Datein in IServ in die Verzeichnisse der Lehrkräfte kopiert, so dass diese Dateien dort automatisch erscheinen. Dieses Skript ist hier jedoch nicht dabei.

### untis-unterrichte.csv
Diese Datei wird aus den Lehrerstammdaten in Untis generiert.

```csv
Name;Liste Kl-Leh;Klassen
L1;05F3;05A1,05A2,05A3,05D2,05D3,05E3,05F3,07A2,07A3,07E3,09D3,10A3,10B3,10C3,10D3,11D,13
L2;;05C2,05C3,05F3,06B2,06B3,08A1,08A2,08A3,08C2,08C3,08D2,08D3,08E3,12,13
L3;;06A3,06B3,06C3,07A3,07B3,07C3,07D3,07E3,09A3,09B3,09C3,09D3,09E3,11B,11D,11E,11F,13
L4;10C3;05B2,06E3,09D3,09E3,10A3,10C3,12,13
```

Damit kann man sowohl die Klassenlehrerschaft als auch die Lerngruppen auslesen. Man erhölt den Export unter Lehrer --> Stammdaten, Untis 2022 ist Voraussetzung!

### Einfache Statistik

Manchmal muss man für die Behörden einfache Sachen machen (alle Schüler, die irgendein Kriterium erfüllen).
Diese Statistikfunktionen sind jedoch noch nicht eingebaut und werden wahrscheinlich bei Bedarf spontan programmiert.

## Benutzung

Man ruft die Software in Python auf mit ```python parser.py``` und erhält diese Ausgabe.
Programmiert wird die Software mit Python 3.10, wahrscheinlich gehen alle Versionen ab Python 3.4.

Die Originaldaten muss im unterordner `daten` liegen, einfach die Demodaten 1:1 austauschen.
Die Exporte liegen dann im Ordner `exporte`.

Alle Exporte sind UTF8 codiert.