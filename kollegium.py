from lehrkraft import *
import csv

class Kollegium:
    """
    Diese Klasse speichert alle Schülerinnen und Schüler und gibt Zugriff
    auf Schüler und Statistiken
    """
    def __init__(self):
        self.lehrkraefte = []
 
        self.rohdatenEinlesen()
        self.klassengruppenEinlesen()

        self.debug()

    # Fügt eine Lehrkraft hinzu
    def lHinzu( self, person ):
        # print("+++++ Füge Person #", len(self.personen), "hinzu +++++")
        
        self.lehrkraefte.append( person )
        # print(person.debug())

    def debug( self ):
        for s in self.lehrkraefte:
            s.debug()

    def klassengruppenEinlesen(self):
        with open('daten/untis-unterrichte.csv',  encoding='UTF8', newline='') as csvfile:
            rohdaten = csv.reader(csvfile, delimiter=';', quotechar='"')

            next(rohdaten)                  #erste Zeile überspringen (Überschrift)

            # Das Dateiformat ist wie folgt aufgebaut
            # Name;Liste Kl-Leh;Klassen
            # L1;05F3;05A1,05A2,05A3,05D2,05D3,05E3,13
            # L2;;05C2,05C3,05F3,06B2,06B3,08A1,08A2,12,13
            for row in rohdaten:
                kuerzel = row[0]
                klassenlehrerschaft = row[1]
                klassen = row[2]

                for l in self.lehrkraefte:
                    if l.kuerzel == kuerzel:
                        l.gruppen += klassen.split(',')
                        l.klassenlehrerschaft += klassenlehrerschaft.split(',')
                


    def rohdatenEinlesen( self ):    
        print("lese die CSV ein")
        with open('daten/Export_fuer_Sibank-Parser.csv',  encoding='UTF8', newline='') as csvfile:
        
            rohdaten = csv.reader(csvfile, delimiter=',', quotechar='"')

            next(rohdaten)                  #erste Zeile überspringen (Überschrift)

            #"GEBURTSDATUM","STRAßE","VORNAME","NAME","MOBILTEL","PLZ","ORT","PRIV. EMAIL*","E-MAIL","POSTFACH*","TITEL","VORWAHL","TELEFON","GESCHLECHT","KÜRZEL"

            for row in rohdaten:       
                lehrkraft = Lehrkraft( 
                row[3],                    #Nachname
                row[2],                    #Rufname
                row[0],                     #Geburtsdatum
                row[1],                     #Strasse
                row[5],                     #PLZ
                row[6],                     #Wohnort
                row[13],                     #Geschlecht
                row[10]                    #Namenspraefix (von, zur, ...)
                )

                lehrkraft.kuerzel = row[14] # Kürzel

                self.lHinzu(lehrkraft) # Die Lehrkraft dem Array hinzufügen
