from datetime import date
from person import Person

# TODO Mit super() muss die __init__-Funktion von Person aufgerufen werden, das
# hier ist eine schlechte Vererbungsstrategie :-)
class Lehrkraft(Person):
    """
    Diese Klasse repräsentiert eine Lehrkraft
    """
    def __init__(self, 
            nachname = "Mustermann", 
            
            # der tatsächlich benutze Vorname
            rufname = "Testvorname", 

            geburtsdatum = "01.01.1970",
            strasse = "Musterweg 1",
            postleitzahl = "0000",
            ort = "Musterhausen",
            geschlecht = "w",
            namenspraefix = "vonundzu"

  
            ):
        self.nachname = nachname
        self.geburtsdatum = self.geburtsdatumBerechnen(geburtsdatum)
        self.strasse = strasse
        self.rufname = rufname
        self.postleitzahl = postleitzahl
        self.ort = ort
        self.geschlecht = geschlecht
        self.namenspraefix = namenspraefix
        self.kuerzel = "kuerzel"
        self.gruppen = []
        self.klassenlehrerschaft = []

        # TODO das gehört in die Klasse Person
        self.alter = date.today().year - self.geburtsdatum.year

    def debug(self):
        print("========================================")
        print(self.rufname, self.nachname)
        print("    ---> ", self.strasse, self.postleitzahl, self.ort, "| Geschlecht ", self.geschlecht, "| geboren am", self.geburtsdatum, " => Alter:", self.alter, sep=" ")
        print("Gruppenmitgliedschaften:", self.gruppen)
        print("========================================")
