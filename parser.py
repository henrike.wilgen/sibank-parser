from kollegium import *
from lehrkraft import *
from schuelerschaft import *


kollegas = Kollegium()
schuelerschaft = Schuelerschaft()
schuelerschaft.generiereSchuelerGruppenProSchueler()
schuelerschaft.erzeugeKlassenliste()
schuelerschaft.listeAlleKlasse()
schuelerschaft.erzeugeGruppenliste()




# schuelerschaft.sIdentnummer(2020101414453426)
# schuelerschaft.sIdentnummer(2020101414453432)


# schuelerschaft.debug()

def csvDateiSchreiben( header, daten, dateiname, deli = ';' ):
    '''
    Erzeugt eine CSV-Datei mit den übergebenen Daten

            Parameters:
                    header (list): Die Namen der Spalten
                    daten (list): Die eigentlichen Daten

            Returns:
                    Nothing
    '''
    with open( dateiname, 'w', encoding='UTF8', newline='') as f:
        writer = csv.writer(f, delimiter=deli)

        # write the header
        writer.writerow(header)

        # write the data
        writer.writerows(daten)

def exportiereSchuelergruppenKlassenmappe( gruppenname ):
    """
    Geht durch alle Gruppen durch und erzeugt alle entsprechenden CSV-Exporte
    """

    header = ['Vorname', 'Nachname', 'Geburtsdatum', 'Klasse', 'Geschlecht']

    data = []


    for schueler in schuelerschaft.schuelerEinerGruppe( gruppenname ):
        zeile = []

        zeile.append(schueler.rufname)
        zeile.append(schueler.nachname)
        zeile.append(schueler.geburtsdatum.strftime("%d.%m.%Y"))
        zeile.append(schueler.klasse)
        zeile.append(schueler.geschlecht)


        data.append( zeile )



    with open('exporte/gruppenlisten/schueler_'+gruppenname+'.csv', 'w', encoding='UTF8', newline='') as f:
        writer = csv.writer(f, delimiter=';')

        # write the header
        writer.writerow(header)

        # write the data
        writer.writerows(data)

def exportiereSchuelerWebUntis():
    """
    Erzeugt einen Export für WebUntis, so dass man dort die Stammdaten für Schüler:innen
    einlesen kann. Lehrkräfte werden über Untis eingelesen, daher braucht dies nicht
    programmiert zu werden.
    """

    header = ['Import-ID', 'Vorname', 'Nachnamenszusatz', 'Nachname', 'Klasse', 'Geschlecht', 'Geburtsdatum']
    data = []

    for schueler in schuelerschaft.personen:
        zeile = []

        zeile.append(schueler.identnummer)
        zeile.append(schueler.rufname)
        zeile.append(schueler.nachname)
        zeile.append(schueler.namenspraefix)
        zeile.append(schueler.klasse)
        zeile.append(schueler.geschlecht)
        zeile.append(schueler.geburtsdatum)




        data.append( zeile )

    csvDateiSchreiben( header, data, 'exporte/schueler_webuntis.csv' )

def exportiereLehrerFuerIserv():
    """
    Erzeugt die CSV-Datei, mit der in IServ die Lehrkräfte importiert werden können.
    """
    header = ['Import-ID', 'Account', 'Vorname', 'Nachnamenszusatz', 'Nachname', 'Klasse/Information', 'Gruppen']

    data = []

    # Beispieldaten
    # 28041978-01112006,L1,Marion,de,Müller,MUE,08B2;12

    for lehrer in kollegas.lehrkraefte:
        zeile = []

        zeile.append("123456-654321")   # TODO hier weiß ich nicht, was für eine UID wir in Zukunft nehmen...
        zeile.append(lehrer.kuerzel)
        zeile.append(lehrer.rufname)
        zeile.append("von und zu")      # TODO muss noch vernünftig eingelesen werden
        zeile.append(lehrer.nachname)
        zeile.append(lehrer.kuerzel)

        gruppen = ""

        for gruppe in lehrer.gruppen:
            gruppen += gruppe
            gruppen += ';'

        zeile.append(gruppen)

        data.append( zeile )



    with open('exporte/lehrer_iserv.csv', 'w', encoding='UTF8', newline='') as f:
        writer = csv.writer(f, delimiter=';')

        # write the header
        writer.writerow(header)

        # write the data
        writer.writerows(data)

def exportiereSchuelerFuerIserv():
    """
    Erzeugt die CSV-Datei, mit der in IServ die Schüler importiert werden können.
    """

    header = ['Import-ID', 'Vorname', 'Nachnamenszusatz', 'Nachname', 'Klasse/Information', 'Gruppen']

    data = []

    for schueler in schuelerschaft.personen:
        zeile = []

        zeile.append(schueler.identnummer)
        zeile.append(schueler.rufname)
        zeile.append(schueler.namenspraefix)
        zeile.append(schueler.nachname)
        zeile.append(schueler.klasse)

        gruppen = ""

        for gruppe in schueler.gruppen:
            gruppen += gruppe
            gruppen += ';'

        zeile.append(gruppen)


        data.append( zeile )



    with open('exporte/sus_iserv.csv', 'w', encoding='UTF8', newline='') as f:
        writer = csv.writer(f, delimiter=';')

        # write the header
        writer.writerow(header)

        # write the data
        writer.writerows(data)


def exportiereLehrkraftAdressbuch():
    """
    dieser Code erzeugt eine CSV-Datei, die von einem CardDAV-Server importiert werden kann.
    Ziel ist es, dass zum Beispiel die Schulleitung auf ein Adressbuch zugreifen kann.
    """

    header = ['Name', 'Vorname', 'Geburtsdatum', 'Geschlecht', 'Strasse', 'Postleitzahl', 'Ort']

    data = []


    for lehrer in kollegas.lehrkraefte:
        zeile = []

        zeile.append(lehrer.namenspraefix+lehrer.nachname)
        zeile.append(lehrer.rufname)
        zeile.append(lehrer.geburtsdatum)
        zeile.append(lehrer.geschlecht)
        zeile.append(lehrer.strasse)
        zeile.append(lehrer.postleitzahl)
        zeile.append(lehrer.ort)

        data.append( zeile )



    with open('exporte/adressbuch.csv', 'w', encoding='UTF8', newline='') as f:
        writer = csv.writer(f)

        # write the header
        writer.writerow(header)

        # write the data
        writer.writerows(data)


exportiereSchuelerWebUntis()
exportiereLehrkraftAdressbuch()
exportiereLehrerFuerIserv()
exportiereSchuelerFuerIserv()

for g in schuelerschaft.gruppen:
    exportiereSchuelergruppenKlassenmappe( g )