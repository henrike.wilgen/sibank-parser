from datetime import date


class Person():
    def __init__(self):
        nachname = "Mustermann", 
        rufname = "Testvorname", 
        geburtsdatum = "01.01.1970",
        strasse = "Musterweg 1",
        postleitzahl = "0000",
        ort = "Musterhausen",
        geschlecht = "w",
        namenspraefix = "vonundzu"
        gruppen = []            # Die Gruppen für den IServ-Import
        alter = date.today().year - self.geburtsdatum.year

      
    def geburtsdatumBerechnen(self, geburtsdatum):
        """Erzeugt das Geburtsdatum"""
        # Das Eingabeformat lautet 28.05.2002
        # oder bei Schülern auch... 16.01.07

        jahr = monat = tag = 0

        if(len(geburtsdatum) == 10):
            jahr = int(geburtsdatum[6:10])
            monat = int(geburtsdatum[3:5])
            tag = int(geburtsdatum[0:2])
        else:
            # Wenn das Datum in Kurzform eingelesen wird muss man
            # 2000 addieren, damit aus 17 2017 wird. Sonst interpretiert
            # Python das als das Jahr 17ad...
            jahr = int(geburtsdatum[6:8])+2000
            monat = int(geburtsdatum[3:5])
            tag = int(geburtsdatum[0:2])

        return date( jahr, monat,tag )