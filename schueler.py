from datetime import date
from person import Person


# TODO Mit super() muss die __init__-Funktion von Person aufgerufen werden, das
# hier ist eine schlechte Vererbungsstrategie :-)
class Schueler(Person):
    """
    Diese Klasse repräsentiert einen Schüler
    """

    def __init__(self,
                 nachname="Mustermann",

                 # der tatsächlich benutze Vorname
                 rufname="Testvorname",

                 # die interne Nummer in Sibank
                 identnummer="0000",
                 klasse="xxx",
                 geburtsdatum="01.01.1970",
                 strasse="Musterweg 1",
                 postleitzahl="0000",
                 ort="Musterhausen",
                 geschlecht="w",
                 namenspraefix="vonundzu",
                 bildungsgang="Testbildungsgang",

                 # Der offizielle Vorname ist das, was im Personalausweis steht
                 vorname_offiziell="Mustervorname"
                 ):
        self.nachname = nachname
        self.identnummer = int(identnummer)
        self.jahrgang = self.jahrgangBerechnen(klasse)
        self.rufname = rufname
        self.klasse = klasse
        self.geburtsdatum = self.geburtsdatumBerechnen(geburtsdatum)
        self.strasse = strasse
        self.gruppen = []
        self.postleitzahl = postleitzahl
        self.ort = ort
        self.geschlecht = geschlecht
        self.namenspraefix = namenspraefix
        self.bildungsgang = bildungsgang
        self.vorname_offiziell = vorname_offiziell
        self.schulzweig = self.schulzweigZuweisen(klasse)

        self.alter = date.today().year - self.geburtsdatum.year

    def schulzweigZuweisen(self, klasse):
        if self.jahrgang > 10:
            return "GZ"

        if self.klasse[-1:] is "1":
            return "HZ"

        if self.klasse[-1:] is "2":
            return "RZ"

        if self.klasse[-1:] is "3":
            return "GZ"

    def jahrgangBerechnen(self, klasse):
        # Berechnet den Jahrgang. Beispiel: Klasse 07A2 --> 7
        if klasse == "12":
            return 12
        elif klasse == "13":
            return 13
        else:
            # Die ersten zwei Zeichen nehmen. Das ist dann zum Beipeil 05 oder 09 oder 11
            # Dann zum Integer machen, dann wird als 06 wieder 6, 11 bleibt 11
            return int(klasse[:2])

    def klassenName(self):
        return self.klasse

    def debug(self):
        print("========================================")
        print(self.rufname, self.nachname, "| Jahrgang", self.jahrgang,
              "| Klasse", self.klasse, " | Identnummer: ", self.identnummer, sep=" ")
        print("    ---> ", self.strasse, self.postleitzahl, self.ort, "| Geschlecht ",
              self.geschlecht, "| geboren am", self.geburtsdatum, " => Alter:", self.alter, sep=" ")
        print("Mitglied in den Gruppen", self.gruppen)
        print("========================================")
