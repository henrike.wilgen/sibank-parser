from schueler import *
import csv


class Schuelerschaft:
    """
    Diese Klasse speichert alle Schülerinnen und Schüler und gibt Zugriff
    auf Schüler und Statistiken
    """

    def __init__(self):
        self.personen = []
        self.klassen = []
        self.gruppen = [] # Alle Gruppen, die existieren

        self.rohdatenEinlesen()
        self.leseLEBein()
        self.leseIndiwareEin()

    def sIdentnummer(self, identnummer):
        # Sucht die Person mit der passenden Identnummer
        # mögliche Identnummer: 2020101414453432
        for s in self.personen:
            if s.identnummer == int(identnummer):
                s.debug()
                return

        print("Kein Schüler mit der Nummer", identnummer, "gefunden")

    def exportiereCSVproLerngruppe( self ):
        """
        Pro Gruppe solle eine CSV-Datei exportiert werden. Die Idee ist, dass diese CSV-Dateien
        dann den Kollegen für den Import in Notenbox, Klassenmappe o.Ä. zur Verfügung gestellt werden können
        """
        for s in self.personen: # Platzhaltercode
            if s.identnummer == int(identnummer):
                s.debug()
                return
        return 

    def gruppenHinzu(self, identnummer, gruppen):
        """den bestehen Gruppen werden die neuen Gruppen hinzugefügt"""
        print("Füge diese Gruppen hinzu:", gruppen)

        for s in self.personen:
            if s.identnummer == int(identnummer):
                s.gruppen += gruppen
                print("+"*20)
                s.debug()
                print("+"*20)
                return

    # Fügt einen SuS hinzu
    def sHinzu(self, person):
        # print("+++++ Füge Person #", len(self.personen), "hinzu +++++")

        self.personen.append(person)
        # print(person.debug())

    def debug(self):
        for s in self.personen:
            s.debug()

    def schuelerEinerKlasse(self, klasse):
        # erstelle eine Liste aller Schüler:innen einer Klasse
        liste = []

        for s in self.personen:
            if s.klasse == klasse:
                liste.append(s)

        return liste

    def schuelerEinerGruppe(self, gruppe):
        # erstelle eine Liste aller Schüler:innen einer Gruppe
        liste = []

        for schueler in self.personen:
            # print("Teste",schueler.nachname)
            # print(schueler.gruppen)
            # print(gruppe)
            if gruppe in schueler.gruppen:
                liste.append(schueler)

        return liste

    def klasseBekannt(self, name):
        print("Überprüfe, ob es diese Klasse gibt: ", name)
        print("Aktuelle gibt es", len(self.klassen), "Klassen")

        for k in self.klassen:
            if k.name() == name:
                print("name bekannt")
                return True
            else:
                print("Name", name, "unbekannt")
                return False

    def listeAlleKlasse(self):
        print("listeAlleKlasse --------------------------")
        for k in self.klassen:
            schueler = self.schuelerEinerKlasse(k)
            print("Klassename: ", k, "\that", len(schueler), "Schüler")

    def erzeugeGruppenliste(self):
        """
        erzeugt die eine Liste [] aller Gruppen, in denen wenigstens ein Schüler ist

        Am Ende sind in self.gruppe alle Gruppen ohne Dupletten drin
        """

        print(self.gruppen)


        for s in self.personen:
            for g in s.gruppen:
                if not g in self.gruppen:
                    self.gruppen.append(g)
        print(self.gruppen)
        
    def erzeugeKlassenliste(self):
        for s in self.personen:                     # durch alle Schüler:innen durchgehen
            aktuelleKlasse = s.klasse               # in welcher Klasse ist der aktuelle S

            # print("Aktuelle Anzahl an Klassen:", len(self.klassen), "\tKlasse der S: ", aktuelleKlasse, "\tAktueller Schüler:", s.rufname)

            # die erste Klasse wird einfach hinzugefügt

            if len(self.klassen) == 0:
                print("nada")
                self.klassen.append(aktuelleKlasse)
            else:
                if aktuelleKlasse in self.klassen:
                    # print(aktuelleKlasse, "gibt es schon!!!")
                    pass
                else:
                    #print("füge Klasse der Liste hinzu\t", aktuelleKlasse)
                    self.klassen.append(aktuelleKlasse)

    def generiereSchuelerGruppenProSchueler(self):
        """Generiert die Liste der Gruppen eines Schülers"""
        for s in self.personen:
            gruppen = []

            # Die folgende beiden generieren die Gruppen für die Klassenund Jahrgänge. Das
            # macht Iserv allerdings automatisch, von daher ist dies nicht notwendig
            # gruppen.append("Klasse-"+s.klasse)
            # gruppen.append("JG-"+str(s.jahrgang))

            # zum Beispiel "Schulzweig-GZ"
            gruppen.append("Schulzweig-"+s.schulzweig)

            if not s.jahrgang > 10:
                gruppen.append("Schueler-Sek-I")
            else:
                gruppen.append("Schueler-Sek-II")

            s.gruppen += gruppen

    def leseLEBein(self):
        """
        LEB erzeugt beispielsweise diese Zeile
        2021 / 2022;KU02;05;Kunst;KU;Pflichtf.;;L1;2021051209283742

        Damit gibt es eine eindeutige Zuordnung zwischen Lehrer und Schüler und Fach und Jahrgang.
        """
        with open('daten/kurse_leb.csv', newline='') as csvfile:
            rohdaten = csv.reader(csvfile, delimiter=';')

            next(rohdaten)  # erste Zeile überspringen (Überschrift)
            
            for row in rohdaten:        # jede Zeile ein Schueler
                
                kursname = row[1].replace(" ", "_").replace("/", "_").replace("->", "_")

                # Interne Abkürzung, kann doppelt vorliegen, 
                                        # wenn man beim Eintragen nicht aufpassen kann
                                        # Daher müssen wir davon ausgehen, dass dieser
                                        # String **nicht** eindeutig ist!
                jahrgang = row[2]       # Zum Beispiel "05" für Jahrgang 5
                fachname = row[3].replace(" ", "_").replace("/", "_")
                                        # Zum Beispiel "Kunst"

                # Spalte 4-6 nicht relevant

                lehrkraft = row[7]      # Kürzel der Lehrkraft des Kurses
                uid = row[8]            # Sibank-UID

                kurs = (lehrkraft+"_"+jahrgang+"_"+fachname+"_"+kursname)

                print(kurs)

                print([kurs])
                self.gruppenHinzu(uid, [kurs])




    def leseIndiwareEin(self):
        """
        Liest die Indiwaredaten ein. Das ist eine CSV pro Jahrgang. Das Format ist leider alles andere als ideal
        Ident;Zusatzfeld1;Name;Vorname;Geburtsdatum;Geschlecht;Kurs117;Kurs116;Kurs115;Kurs114;Kurs113;Kurs112;Kurs111;Kurs110;Kurs19;Kurs18;Kurs17;Kurs16;Kurs15;Kurs14;Kurs13;Kurs12;Kurs11;
        Die ersten beiden Spalten sind zwei UIDs, wobei die zweite die von Sibank ist.

        Spalten 3-6 sind offensichtlich

        Danach kommen die Kurse des Schülers. Aus diesen müssen dann die Gruppen gebaut werden
        """
        with open('daten/indiware12.csv', newline='') as csvfile:
            rohdaten = csv.reader(csvfile, delimiter=';')

            next(rohdaten)  # erste Zeile überspringen (Überschrift)

            for row in rohdaten:        # jede Zeile ein Schueler
                uid = row[1]            # Sibank-UID                

                gruppennamen = [  # Indiware hat hier bis zu 17 Gruppen, beginnend mit Spalte 7
                    row[6],
                    row[7],
                    row[8],
                    row[9],
                    row[10],
                    row[11],
                    row[12],
                    row[13],
                    row[14],
                    row[15],
                    row[16],
                    row[17],
                    row[18],
                    row[19],
                    row[20],
                    row[21],
                    row[22]
                ]

                # Alle non-empty-Strings löschen
                nichtLeereGruppen = [var for var in gruppennamen if var]

                print("Schüler", uid, "belegt die Gruppen ", nichtLeereGruppen)

                self.gruppenHinzu(uid, nichtLeereGruppen)

    def rohdatenEinlesen(self):
        print("lese die CSV ein")
        with open('daten/Datenexport_alle_aktiven_SuS.csv', encoding='UTF8', newline='') as csvfile:

            rohdaten = csv.reader(csvfile, delimiter=',', quotechar='"')

            next(rohdaten)  # erste Zeile überspringen (Überschrift)

            for row in rohdaten:
                student = Schueler(
                    row[1],  # Familienname
                    row[11],  # Rufname
                    row[0],  # Identnummer
                    row[8],  # Klasse
                    row[3],  # Geburtsdatum
                    # Geburtsort ist die 4, wird noch nicht genutzt
                    row[5],  # Strasse
                    row[6],  # PLZ
                    row[7],  # Wohnort
                    row[9],  # Geschlecht
                    row[10],  # Namenspraefix (von, zur, ...)
                    row[12],  # Bilddungsgang
                    row[2]  # offizieller Vornames
                )

                self.sHinzu(student)  # Den Schüler dem Array hinzufügen
